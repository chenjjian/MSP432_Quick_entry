/****************************************************/
// MSP432P401R
// 7 Uart中断 接收Bluetooth数据 参考示例
// Bilibili：m-RNA
// E-mail:m-RNA@qq.com
// 创建日期:2022/07/18
/****************************************************/

#include "sysinit.h"
#include "usart.h"
#include "led.h"
#include "tim32.h"

u32 HAL_GetTick(void);

/*********   硬件底层    *********/
static vu32 SW_Timer_Tick = 0; // 软件定时器

#define BLUETOOTH_UART EUSCI_A1_BASE    // 蓝牙使用串口
#define BLUETOOTH_UART_RX_LENGTH_MAX 50 // 蓝牙最大接收缓存长度

static uint8_t Bluetooth_Uart_Rx_Index = 0;                           // UART接收计数索引
static uint8_t Bluetooth_Uart_Rx_Temp = 0;                            // UART中断接收缓存
uint8_t Bluetooth_Uart_Rx_Buffer[BLUETOOTH_UART_RX_LENGTH_MAX] = {0}; // UART接收缓存

/*********   应用层   *********/

uint8_t Bluetooth_Rx_Data[4] = {0};           // 接收Bluetooth数据
uint8_t Bluetooth_Rx_Data_Analysis_State = 0; // Bluetooth数据解析状态

void Bluetooth_Check_Data_Task(void)
{
    uint16_t i;

    if (!Bluetooth_Uart_Rx_Index) // 没有数据 退出
        return;

    if (SW_Timer_Tick > HAL_GetTick())
        return; // 20ms未到 退出

    // 接收长度是否达到
    if (Bluetooth_Uart_Rx_Index < 3) // 按需修改 （3：一个u8； 4：两个u8；依此类推）
        return;

    // 检查格式是否正确 格式 {u8}
    if ((Bluetooth_Uart_Rx_Buffer[0] != '{') &&                         // 帧头为 {
        (Bluetooth_Uart_Rx_Buffer[Bluetooth_Uart_Rx_Index - 1] != '}')) // 帧尾为 }
        goto Send_Error;

    // 命令正确
    for (i = 0; i < ((Bluetooth_Uart_Rx_Index - 2)); ++i)
    {
        Bluetooth_Rx_Data[i] = Bluetooth_Uart_Rx_Buffer[i];
    }
    Bluetooth_Rx_Data_Analysis_State = 1; // 解析状态置1

    goto Jump; // 正常流程就跳过
Send_Error:    // 解析错误
    // do something ...

    printf_uart1("?"); // Bluetooth回复 "?"

    printf("[Log] Bluetooth Rx Data ERROR\r\n"); // 打印解析出错
Jump:
    // 清零接收缓冲 为下一次接收准备
    Bluetooth_Uart_Rx_Index = 0;
    memset(Bluetooth_Uart_Rx_Buffer, 0, BLUETOOTH_UART_RX_LENGTH_MAX);
}

// Uart1接收中断
void EUSCIA1_IRQHandler(void)
{
    uint32_t status = MAP_UART_getEnabledInterruptStatus(BLUETOOTH_UART);
    if (status & EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG) //接收中断
    {
        MAP_UART_clearInterruptFlag(BLUETOOTH_UART, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG); // 清除中断标志位

        Bluetooth_Uart_Rx_Temp = MAP_UART_receiveData(BLUETOOTH_UART); // 临时接收

        if (Bluetooth_Uart_Rx_Index < BLUETOOTH_UART_RX_LENGTH_MAX) // 接收缓冲是否溢出
        {
            SW_Timer_Tick = HAL_GetTick() + 20; // 软件定时复位20ms

            Bluetooth_Uart_Rx_Buffer[Bluetooth_Uart_Rx_Index] = Bluetooth_Uart_Rx_Temp; // 放入缓存
            Bluetooth_Uart_Rx_Index++;                                                  // 索引加1
        }
        else // 强制标记接收完成
        {
            SW_Timer_Tick = HAL_GetTick() - 1;
        }
    }
}

int main(void)
{
    SysInit();          // 第3讲 时钟配置
    uart0_init(115200); // 第7讲 串口配置
    uart1_init(9600);   // 第7讲 串口配置(蓝牙)

    // 不分频，ARR为48000，周期为48000000 / 48000 = 1ms 作为时基
    Tim32_1_Int_Init(48000, TIMER32_PRESCALER_1);

    printf("[Log] Init Completed! \r\n");
    printf("[Log] Uart1 接收数据格式为 \"{\" + u8 + \"}\" \r\n");

    MAP_Interrupt_enableMaster(); // 开启总中断

    while (1)
    {
        Bluetooth_Check_Data_Task();

        if (Bluetooth_Rx_Data_Analysis_State)
        {
            // 打印一下接收数据
            printf("[Bluetooth Data] x1: %d\r\n", Bluetooth_Rx_Data[0]); //, Bluetooth_Rx_Data[1]);

            // 清除解析完成状态 清除缓存
            Bluetooth_Rx_Data_Analysis_State = 0;
            memset(Bluetooth_Rx_Data, 0, sizeof(Bluetooth_Rx_Data));
        }
    }
}

/*********   时基部分    *********/

/* Timer32 ISR */
static vu32 uwTick = 0;
void T32_INT2_IRQHandler(void)
{
    MAP_Timer32_clearInterruptFlag(TIMER32_1_BASE);

    /*开始填充用户代码*/

    uwTick++; // 1ms加1

    /*结束填充用户代码*/
}

// 对外开发函数
u32 HAL_GetTick(void)
{
    return uwTick;
}
